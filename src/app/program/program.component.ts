import { Component} from '@angular/core';
import {Http, Response} from '@angular/http'; 

@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.css']
  
})
export class ProgramComponent {
   csvUrl: string = './program.csv';  
   csvData: any[] = [];
   kino_object = { 
    movie: "", 
    time: "",
    day:"",
    theatre:"",
    price:""
 };
  picsoff = false;

   constructor (private http: Http) {}

   readCsvData () {
     this.http.get(this.csvUrl)
     .subscribe(
       data => this.extractData(data),
       err => this.handleError(err)
     );
   }

   private extractData(res: Response) {

    
     let csvData = res['_body'] || '';
     let allTextLines = csvData.split(/\r\n|\n/);
     let headers = allTextLines[0].split(',');
     let lines = [];

     for ( let i = 0; i < allTextLines.length; i++) {
         let data = allTextLines[i].split(',');
         if (data.length == headers.length) {
             let tarr = [];
             for ( let j = 0; j < headers.length; j++) {
                 tarr.push(data[j]);
             }
             lines.push(tarr);
         }
     }
     this.csvData = lines;
   }

   private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
       error.status ? `${error.status} - ${error.statusText}` : 'Server error';
     console.error(errMsg); 
     return errMsg;

   }

}
