import {Routes, RouterModule} from "@angular/router";
import { ProgramComponent } from "./program/program.component";
import { SeatingComponent } from "./seating/seating.component";
import { PaymentComponent } from "./payment/payment.component";
import { ConfirmationComponent } from "./confirmation/confirmation.component";

const APP_ROUTES: Routes = [
{path:'program', component: ProgramComponent},
{path: 'seating', component: SeatingComponent},
{path: 'payment', component: PaymentComponent},
{path: 'confirmation', component: ConfirmationComponent}

];

export const routing = RouterModule.forRoot(APP_ROUTES);