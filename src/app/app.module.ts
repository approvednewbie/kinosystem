import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ProgramComponent } from './program/program.component';
import { SeatingComponent } from './seating/seating.component';
import { PaymentComponent } from './payment/payment.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { routing } from './app.routing';
import {FormsModule} from "@angular/forms";
import { HttpModule, JsonpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    ProgramComponent,
    SeatingComponent,
    PaymentComponent,
    ConfirmationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, routing, HttpModule, JsonpModule

   

  ],
  providers: [],
  bootstrap: [AppComponent, ProgramComponent]
})
export class AppModule { }
