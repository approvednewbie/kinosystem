import { Component, OnInit, Output, Input } from '@angular/core';
import { NgForm } from '../../../node_modules/@angular/forms';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  @Input()
  user = {
    name: 'rajiv',
    cccompany: 'Visa',
    ccnumber: '12345',
    expdate: '12-0012',
    cvc:'123',
  };

  form: { name: string; cccompany: string; ccnumber: string; expdate: string; cvc: string; };

  constructor() {}
  
  onSubmit(form : NgForm) {
    this.form = this.user;
    console.log(form);}

  ngOnInit() {}
  }
